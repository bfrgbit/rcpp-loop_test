/***
 * func_cpp.cpp
 *
 * Created: 2018-2-2
 *  Author: Charles Zhu
 */

#include <Rcpp.h>
using namespace Rcpp;

// [[Rcpp::export]]
IntegerVector up_to_cpp(IntegerVector n) {
    if (n.length() != 1) {
        throw(std::length_error("n.length() != 1"));
    }
    if (n[0] < 0) {
        throw(std::range_error("n < 0"));
    }

    IntegerVector sum(1);
    IntegerVector ind(1);

    sum[0] = 0;
    for (ind[0]= 1; ind[0] <= n[0]; ++ind[0]) {
        sum[0] += ind[0];
    }
    return sum;
}
